<?php

/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@web', '/');

return [
    // Whether to delete asset source after compression:
    'deleteSource' => true,
    // The list of asset bundles to compress:
    'bundles' => [
        backend\assets\AppAsset::class,
        yii\web\YiiAsset::class,
        yii\web\JqueryAsset::class,
        yii\validators\ValidationAsset::class,
        yii\widgets\ActiveFormAsset::class,
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
            'class' => yii\web\AssetBundle::class,
            'basePath' => '@backend/web/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/all-{hash}.js',
            'css' => 'css/all-{hash}.css',
        ],
        'forms' => [
            'class' => yii\web\AssetBundle::class,
            'basePath' => '@backend/web/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/forms-{hash}.js',
            'css' => 'css/forms-{hash}.css',
            'depends' => [
                yii\validators\ValidationAsset::class,
                yii\widgets\ActiveFormAsset::class,
            ],
        ]
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@backend/web/assets',
        'baseUrl' => '@web/assets',
    ],
];
