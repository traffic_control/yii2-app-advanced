<?php

namespace backend\assets;

use yii\bootstrap5\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\YiiAsset;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@backend/assets/app';

    public $css = [
        'css/site.css',
    ];

    public $js = [
    ];

    public $depends = [
        YiiAsset::class,
        BootstrapAsset::class,
    ];
}
