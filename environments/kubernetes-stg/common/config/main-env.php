<?php

return [
    'components' => [
        'redis' => [
            'class' => yii\redis\Connection::class,
            'hostname' => getenv('REDIS_MASTER_HOST'),
            'password' => getenv('REDIS_PASSWORD'),
            'database' => 1
        ],
        'redisReplica' => [
            'class' => yii\redis\Connection::class,
            'hostname' => getenv('REDIS_REPLICA_HOST'),
            'password' => getenv('REDIS_PASSWORD'),
            'database' => 1
        ],
        'db' => [
            'class' => yii\db\Connection::class,
            'dsn' => 'mysql:host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME'),
            'username' => getenv('DB_USER'),
            'password' => getenv('DB_PASSWORD'),
            'charset' => 'utf8',
            'enableSchemaCache' => true,
            'on afterOpen' => function ($event) {
                $event->sender->createCommand("SET time_zone = '+00:00'")->execute();
            },
            'schemaMap' => [
                'mysql' => SamIT\Yii2\MariaDb\Schema::class
            ],
        ],
        'mutex' => [
            'class' => yii\redis\Mutex::class,
            'expire' => 600,
            'redis' => 'redis',
        ],
        'cache' => [
            'class' => yii\redis\Cache::class,
            'keyPrefix' => 'cache_',
            'redis' => 'redis',
            'shareDatabase' => true,
            'replicas' => [
                ['redisReplica']
            ]
        ],
    ],
];
