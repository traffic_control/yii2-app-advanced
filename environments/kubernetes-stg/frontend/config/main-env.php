<?php

return [
    'bootstrap' => ['debug'],
    'modules' => [
        'debug' => [
            'class' => yii\debug\Module::class,
            'allowedIPs' => ['*'],
        ],
        'health' => [
            'components' => [
                'redis' => [
                    'class' => trafficcontrol\yii\health\components\redis\Redis::class,
                ],
                'redis-replica' => [
                    'class' => trafficcontrol\yii\health\components\redis\Redis::class,
                    'redis' => 'redisReplica',
                ],
            ],
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'trustedHosts' => [
                '10.0.0.0/8' => ['forwarded']
            ],
        ],
        'session' => [
            'class' => yii\web\CacheSession::class,
            'cache' => [
                'class' => yii\redis\Cache::class,
                'keyPrefix' => 'frontend_',
                'redis' => 'redis',
                'replicas' => [
                    ['redisReplica']
                ]
            ]
        ],
        'assetManager' => [
            'bundles' => require('assets-bundle.php')
        ],
    ],
];
