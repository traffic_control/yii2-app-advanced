<?php

/**
 * The manifest of files that are local to specific environment.
 * This file returns a list of environments that the application
 * may be installed under. The returned data must be in the following
 * format:
 *
 * ```php
 * return [
 *     'environment name' => [
 *         'path' => 'directory storing the local files',
 *         'skipFiles'  => [
 *             // list of files that should only be copied once and skipped if they already exist
 *         ],
 *         'setWritable' => [
 *             // list of directories that should be set writable
 *         ],
 *         'setExecutable' => [
 *             // list of files that should be set executable
 *         ],
 *         'setCookieValidationKey' => [
 *             // list of config files that need to be inserted with automatically generated cookie validation keys
 *         ],
 *         'createSymlink' => [
 *             // list of symlinks to be created. Keys are symlinks, and values are the targets.
 *         ],
 *     ],
 * ];
 * ```
 */

return [
    'Development' => [
        'path' => 'dev',
        'setWritable' => [
            'backend/runtime',
            'backend/web/assets',
            'console/runtime',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'skipFiles' => [
            'backend/config/main-local.php',
            'backend/config/test-local.php',
            'frontend/config/main-local.php',
            'frontend/config/test-local.php',
            'console/config/main-local.php',
            'console/config/test-local.php',
            'common/config/main-local.php',
            'common/config/test-local.php',
        ],
        'setExecutable' => [
            'yii',
            'yii_test',
        ],
        'setCookieValidationKey' => [
            'backend/config/main-env.php',
            'frontend/config/main-env.php',
        ],
    ],
    'Test' => [
        'path' => 'test',
        'setWritable' => [
            'backend/runtime',
            'backend/web/assets',
            'console/runtime',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'setExecutable' => [
            'yii',
            'yii_test',
        ],
        'setCookieValidationKey' => [
            'backend/config/main-env.php',
            'frontend/config/main-env.php',
        ],
    ],
    'Kubernetes-staging' => [
        'path' => 'kubernetes-stg',
        'setWritable' => [
            'backend/runtime',
            'backend/web/assets',
            'console/runtime',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'setExecutable' => [
            'yii',
            'yii2-apache-foreground',
        ],
    ],
    'Kubernetes-production' => [
        'path' => 'kubernetes',
        'setWritable' => [
            'backend/runtime',
            'backend/web/assets',
            'console/runtime',
            'frontend/runtime',
            'frontend/web/assets',
        ],
        'setExecutable' => [
            'yii',
            'yii2-apache-foreground',
        ],
    ],
    // 'Production' => [
    //     'path' => 'prod',
    //     'setWritable' => [
    //         'backend/runtime',
    //         'backend/web/assets',
    //         'console/runtime',
    //         'frontend/runtime',
    //         'frontend/web/assets',
    //     ],
    //     'setExecutable' => [
    //         'yii',
    //     ],
    //     'setCookieValidationKey' => [
    //         'backend/config/main-local.php',
    //         'frontend/config/main-local.php',
    //     ],
    // ],
];
