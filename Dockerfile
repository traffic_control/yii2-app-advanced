FROM registry.gitlab.com/traffic_control/docker-containers/php:8.1-apache

RUN curl -sSLf -o /usr/local/bin/install-php-extensions https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
    && chmod +x /usr/local/bin/install-php-extensions \
    && IPE_GD_WITHOUTAVIF=1 install-php-extensions gd \
    && rm /usr/local/bin/install-php-extensions

ARG COMPOSER_AUTH_JSON
ARG YII2_INIT_ENVIRONMENT="Kubernetes-production"
ARG COMPOSER_EXTRA_ARGS="--no-dev"

COPY ./ /var/www/html

RUN if [ ! -z $COMPOSER_AUTH_JSON ] ; then mkdir -p /root/.composer ; echo $COMPOSER_AUTH_JSON > ~/.composer/auth.json ; fi \
    && composer install --no-ansi --no-interaction --optimize-autoloader \
    && ./init --env=$YII2_INIT_ENVIRONMENT --overwrite=A \
    && touch common/config/main-local.php \
    && touch console/config/main-local.php \
    && ./yii assets/compress frontend/config/assets.php frontend/config/assets-bundle.php \
    && ./yii assets/compress backend/config/assets.php backend/config/assets-bundle.php \
    && rm common/config/main-local.php \
    && rm console/config/main-local.php \
    && composer install --no-ansi --no-interaction --optimize-autoloader $COMPOSER_EXTRA_ARGS \
    && composer clear-cache \