<?php

/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
Yii::setAlias('@web', '/');

return [
    // Whether to delete asset source after compression:
    'deleteSource' => true,
    // The list of asset bundles to compress:
    'bundles' => [
        frontend\assets\AppAsset::class,
        yii\bootstrap5\BootstrapAsset::class,
        yii\bootstrap5\BootstrapPluginAsset::class,
        yii\captcha\CaptchaAsset::class,
        yii\validators\ValidationAsset::class,
        yii\web\YiiAsset::class,
        yii\web\JqueryAsset::class,
        yii\widgets\ActiveFormAsset::class,
    ],
    // Asset bundle for compression output:
    'targets' => [
        'all' => [
            'class' => yii\web\AssetBundle::class,
            'basePath' => '@frontend/web/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/all-{hash}.js',
            'css' => 'css/all-{hash}.css',
        ],
        'forms' => [
            'class' => yii\web\AssetBundle::class,
            'basePath' => '@frontend/web/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/forms-{hash}.js',
            'css' => 'css/forms-{hash}.css',
            'depends' => [
                yii\validators\ValidationAsset::class,
                yii\widgets\ActiveFormAsset::class,
            ],
        ],
        'captcha' => [
            'class' => yii\web\AssetBundle::class,
            'basePath' => '@frontend/web/assets',
            'baseUrl' => '@web/assets',
            'js' => 'js/captcha-{hash}.js',
            'css' => 'css/captcha-{hash}.css',
            'depends' => [
                yii\captcha\CaptchaAsset::class,
            ]
        ]
    ],
    // Asset manager configuration:
    'assetManager' => [
        'basePath' => '@frontend/web/assets',
        'baseUrl' => '@web/assets',
    ],
];
