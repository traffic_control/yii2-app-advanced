stages:
  - codequality
  - tests
  - build-common
  - build
  - deploy
  - cleanup

include:
  - local: '/.gitlab-ci/kubernetes.yml'

.test-shared: &test-shared
  stage: tests
  coverage: /^\s*Lines:\s*(\d+.\d+)\%/
  # Select what we should cache between builds
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - vendor/
  # Bring in any services we need http://docs.gitlab.com/ee/ci/docker/using_docker_images.html#what-is-a-service
  # See http://docs.gitlab.com/ce/ci/services/README.html for examples.
  services:
    - name: mariadb:10.4
      alias: mariadb
  variables:
    FF_NETWORK_PER_BUILD: "1"
    MYSQL_DATABASE: "yii2advanced"
    MYSQL_USER: "yii2advanced"
    MYSQL_PASSWORD: "secret"
    MYSQL_RANDOM_ROOT_PASSWORD: "yes"
  artifacts:
    paths:
      - frontend/tests/_output
      - backend/tests/_output
      - common/tests/_output
    expire_in: 1 week
    when: always
    reports:
      junit: common/tests/_output/report.xml
  before_script:
    - if [[ ! -z $COMPOSER_AUTH_JSON ]] ; then mkdir -p /root/.composer ; echo $COMPOSER_AUTH_JSON > ~/.composer/auth.json ; fi
    - composer install --prefer-dist
    - php init --env=Test --overwrite=All
    - php yii migrate --interactive=0

.build-shared: &build-shared
  # Use the official docker image.
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  dependencies: []
  before_script:
    - mkdir -p /kaniko/.docker
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH
    # - if: '$CI_COMMIT_BRANCH == "master"' // Add this rule, if you are using git-flow avh
    #   when: never

phpcs:
  stage: codequality
  image: registry.gitlab.com/traffic_control/docker-containers/php:tc-phpcs
  script:
    - phpcs --extensions=php ./

test:8.1:
  <<: *test-shared
  image: registry.gitlab.com/traffic_control/docker-containers/php:8.1-cli-pcov
  script:
    - php -dpcov.exclude="~vendor~" vendor/bin/codecept run --coverage --coverage-html --xml --html --no-colors

test:8.0:
  <<: *test-shared
  image: registry.gitlab.com/traffic_control/docker-containers/php:8.0-cli-pcov
  script:
    - php -dpcov.exclude="~vendor~" vendor/bin/codecept run --coverage --coverage-html --xml --html --no-colors

# test:7.4:
#   <<: *test-shared
#   image: registry.gitlab.com/traffic_control/docker-containers/php:7.4-cli-pcov
#   script:
#     - php -dpcov.exclude="~vendor~" vendor/bin/codecept run --coverage --coverage-html --xml --html --no-colors

build-common:
  <<: *build-shared
  stage: build-common
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      YII2_INIT_ENVIRONMENT="Kubernetes-staging"
      if [[ ! -z "$CI_COMMIT_TAG" ]]; then
        tag="$CI_COMMIT_TAG"
        COMPOSER_EXTRA_ARGS="--no-dev"
        YII2_INIT_ENVIRONMENT="Kubernetes-prod"
        echo "Running on tag '$CI_COMMIT_TAG': tag = $tag"
      elif [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag="latest"
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag="$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - "/kaniko/executor
      --context ${CI_PROJECT_DIR}
      --build-arg COMPOSER_EXTRA_ARGS=$COMPOSER_EXTRA_ARGS
      --build-arg COMPOSER_AUTH_JSON=$COMPOSER_AUTH_JSON
      --build-arg YII2_INIT_ENVIRONMENT=$YII2_INIT_ENVIRONMENT
      --dockerfile ${CI_PROJECT_DIR}/Dockerfile
      --destination $CI_REGISTRY_IMAGE/base:${tag}"

build-entrypoints:
  <<: *build-shared
  stage: build
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      if [[ ! -z "$CI_COMMIT_TAG" ]]; then
        tag="$CI_COMMIT_TAG"
        echo "Running on tag '$CI_COMMIT_TAG': tag = $tag"
      elif [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag="latest"
        echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
      else
        tag="$CI_COMMIT_REF_SLUG"
        echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
      fi
    - "/kaniko/executor
      --context ${CI_PROJECT_DIR}
      --build-arg BASE_IMAGE=$CI_REGISTRY_IMAGE/base:${tag}
      --dockerfile ${CI_PROJECT_DIR}/${ENTRYPOINT}/Dockerfile
      --destination $CI_REGISTRY_IMAGE/${ENTRYPOINT}:${tag}"
  parallel:
    matrix:
      - ENTRYPOINT: frontend
      - ENTRYPOINT: backend
